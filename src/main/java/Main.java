import java.sql.*;
import java.util.Properties;

public class Main {
    static Connection con;

    public static void main(String[] args) {

        Articulos articulo = new Articulos("Monitor 47", (float) 170.0, "Mon47", 1, 0, "Hardware");
        conectarBD();

        if (con != null) {

            System.out.println("id - nombre - precio - codigo - stock - grupo - descripcion");
            System.out.println("--------------------------------------------------------");
            muestraArticulos();

            insertarArticulo(articulo);

            desconectarBD();
        }

    }

    private static void muestraArticulos(){

        Articulos art;

        String sqlConsulta = "SELECT * \n" +
                "FROM empresa_ad.articulos \n" +
                "INNER JOIN empresa_ad.grupos ON articulos.grupo = grupos.id ORDER BY empresa_ad.articulos.id";

        try (Statement st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
             ResultSet rs = st.executeQuery(sqlConsulta)) {

            rs.last();
            rs.beforeFirst();
            while (rs.next()) {
                art = leerRegistroCliente(rs);
                System.out.println(art);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public static Articulos insertarArticulo(Articulos art){
        String selectInsert = "insert into empresa_ad.articulos(nombre, precio, codigo, grupo, stock) values ('" +
                art.getNombre() + "'" + ", " + "'" + art.getPrecio() + "'" + ", " + "'" + art.getCodigo() + "'" + ", "
                + "'" + art.getStock() + "'" + ", " + "'" + art.getGrupo() + "')";

        try(Statement st = con.createStatement()){
            st.executeUpdate(selectInsert, Statement.RETURN_GENERATED_KEYS);

            ResultSet rsClaves = st.getGeneratedKeys();
            rsClaves.next();
            art.setId(rsClaves.getInt(1));
            return art;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }



        private static void conectarBD() {
        try {
            String jdbcURL = "jdbc:mariadb://192.168.56.101:3306/empresa_ad";
            //String jdbcURL = "jdbc:postgresql://192.168.56.101:5432/batoi?schema=empresa_ad";
            Properties pc = new Properties();
            pc.put("user", "batoi");
            pc.put("password", "1234");
            con = DriverManager.getConnection(jdbcURL, pc);
        } catch (SQLException ex) {
            System.err.println("Conectando a la base de datos..." + ex.getMessage());
        }
    }

    private static void desconectarBD() {
        try {
            con.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    private static Articulos leerRegistroCliente(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String nombre = rs.getString("nombre");
        float precio = rs.getFloat("precio");
        String codigo = rs.getString("codigo");
        int stock = rs.getInt("stock");
        int grupo = rs.getInt("grupo");
        String descripcion = rs.getString("descripcion");
        return new Articulos(id, nombre, precio, codigo, stock, grupo, descripcion);
    }


    private static void recorridoConResultSet() {
        Articulos art;

        String sqlConsulta = "SELECT * FROM empresa_ad.articulos";

        try (Statement st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
             ResultSet rs = st.executeQuery(sqlConsulta)) {

            rs.last();
            System.out.println("Total registros " + rs.getRow());
            System.out.println("Listado de clientes");
            rs.beforeFirst();
            while (rs.next()) {
                art = leerRegistroCliente(rs);
                System.out.println(art);
            }
            System.out.println("-------------------");

            System.out.println("Ultimo registro");
            rs.last();
            art = leerRegistroCliente(rs);
            System.out.println(art);
            System.out.println("-------------------");

            System.out.println("Anterior registro");
            rs.previous();
            art = leerRegistroCliente(rs);
            System.out.println(art);
            System.out.println("-------------------");

            System.out.println("Salto absoluto al registro 2");
            rs.absolute(2);
            art = leerRegistroCliente(rs);
            System.out.println(art);
            System.out.println("-------------------");

            System.out.println("Salto relativo dos posiciones adelante");
            rs.relative(2);
            art = leerRegistroCliente(rs);
            System.out.println(art);
            System.out.println("");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
